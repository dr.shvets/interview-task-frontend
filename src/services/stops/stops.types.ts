interface BusStop {
  line: number;
  stop: string;
  order: number;
  time: string;
}

interface BusStopGroup {
  name: string;
  order: number;
  stops: BusStop[];
}

type GetStopListResponse = BusStop[];

type LinesList = {
  [line: number]: Record<string, BusStopGroup>;
};

export { BusStop, GetStopListResponse, LinesList, BusStopGroup };
