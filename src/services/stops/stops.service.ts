import { apiClient } from '@/api/api-client';
import { stopsEndpoints } from '@/services/stops/stops.endpoints';
import { GetStopListResponse } from '@/services/stops/stops.types';

export function fetchStops() {
  return new Promise((resolve, reject) => {
    apiClient
      .get<GetStopListResponse>(stopsEndpoints.GET_STOPS)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
