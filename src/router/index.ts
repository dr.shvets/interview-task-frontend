import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import { RouteNames } from './route-names';
import TimeTableLayout from '@/layouts/TimeTableLayout.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '',
    name: RouteNames.TIMETABLE_ROOT,
    component: TimeTableLayout,
    redirect: { name: RouteNames.BUS_LINES },
    children: [
      {
        path: '/',
        name: RouteNames.BUS_LINES,
        component: () => import('@/views/BusLinesView.vue'),
      },
      {
        path: 'stops',
        name: RouteNames.BUS_STOPS,
        component: () => import('@/views/BusStopsView.vue'),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
