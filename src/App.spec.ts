import { shallowMount } from '@vue/test-utils';
import App from './App.vue';

describe('App', () => {
  const createComponent = () => {
    return shallowMount(App);
  };

  let wrapper: ReturnType<typeof createComponent>;

  it('renders app component', () => {
    wrapper = createComponent();
    expect(wrapper.exists()).toBe(true);
  });
});
