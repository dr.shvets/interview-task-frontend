import axios from 'axios';

export const apiClient = axios.create({
  baseURL: process.env.VUE_APP_BACKEND_URL,
  timeout: 1000,
  headers: { 'Content-Type': 'application/json' },
});
