import { State } from './state';
import { MutationTree } from 'vuex';
import { GetStopListResponse } from '@/services/stops/stops.types';

export enum MutationTypes {
  SET_STOPS = 'SET_STOPS',
  SET_LOADING = 'SET_LOADING',
  SET_ERROR = 'SET_ERROR',
}

export type Mutations<S = State> = {
  [MutationTypes.SET_STOPS](state: S, payload: GetStopListResponse): void;
  [MutationTypes.SET_LOADING](state: S, payload: boolean): void;
  [MutationTypes.SET_ERROR](state: S, payload: boolean): void;
};

export const mutations: MutationTree<State> & Mutations<State> = {
  [MutationTypes.SET_STOPS](state, payload) {
    state.stops = payload;
  },
  [MutationTypes.SET_LOADING](state, payload) {
    state.loading = payload;
  },
  [MutationTypes.SET_ERROR](state, payload) {
    state.error = payload;
  },
};
