import { GetterTree } from 'vuex';
import { State } from './state';
import {
  LinesList,
  GetStopListResponse,
  BusStopGroup,
} from '@/services/stops/stops.types';

export type Getters = {
  linesList: (state: State) => LinesList;
  stopsList: (state: State) => string[];
};

export const getters: GetterTree<State, State> & Getters = {
  linesList: (state) => regroupBusStops(state.stops),
  stopsList: (state) => {
    const stopCollection = new Set<string>();

    for (const busStop of state.stops) {
      stopCollection.add(busStop.stop);
    }

    return Array.from(stopCollection).sort();
  },
};

function regroupBusStops(data: GetStopListResponse): LinesList {
  const groupedLines = new Map<number, Record<string, BusStopGroup>>();

  for (const busStop of data) {
    let line = groupedLines.get(busStop.line);

    if (!line) {
      line = {};

      Object.defineProperty(line, busStop.stop, {
        value: {
          name: busStop.stop,
          order: busStop.order,
          stops: [],
        },
      });
    }

    if (!line[busStop.stop]) {
      line[busStop.stop] = {
        name: busStop.stop,
        order: busStop.order,
        stops: [],
      };
    }

    line[busStop.stop].stops.push(busStop);
    groupedLines.set(busStop.line, line);
  }

  return Object.fromEntries(groupedLines.entries());
}
