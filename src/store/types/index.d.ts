import { Store } from 'vuex';

export interface MutationTree<S> {
  [key: string]: Mutation<S>;
}

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: Store<State>;
  }
}
