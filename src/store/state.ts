import { GetStopListResponse } from '@/services/stops/stops.types';

export const state = {
  stops: [] as GetStopListResponse,
  loading: false,
  error: false,
};

export type State = typeof state;
