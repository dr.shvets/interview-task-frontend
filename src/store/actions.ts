import { GetStopListResponse } from '@/services/stops/stops.types';
import { State } from './state';
import { Mutations, MutationTypes } from './mutations';
import { ActionTree, ActionContext } from 'vuex';
import { fetchStops } from '@/services/stops/stops.service';

export enum ActionTypes {
  FETCH_STOPS = 'FETCH_STOPS',
}

export interface Actions {
  [ActionTypes.FETCH_STOPS]({ commit }: AugmentedActionContext): void;
}

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, State>, 'commit'>;

export const actions: ActionTree<State, State> & Actions = {
  async [ActionTypes.FETCH_STOPS]({ commit }) {
    try {
      commit(MutationTypes.SET_LOADING, true);
      const stops = (await fetchStops()) as GetStopListResponse;
      commit(MutationTypes.SET_STOPS, stops);
    } catch {
      commit(MutationTypes.SET_ERROR, true);
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },
};
