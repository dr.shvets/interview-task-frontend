import { shallowMount } from '@vue/test-utils';
import VButton from './VButton.vue';
import { SIZE_CLASS, COLOR_CLASS } from './button.constants';

describe('Button', () => {
  const createComponent = (props: Record<string, unknown> = {}) => {
    return shallowMount(VButton, {
      props: {
        size: 'md',
        color: 'primary',
        ...props,
      },
    });
  };

  let wrapper: ReturnType<typeof createComponent>;

  it('renders button component', () => {
    wrapper = createComponent();

    expect(wrapper.exists()).toBe(true);
  });

  it('emits click event', () => {
    wrapper.trigger('click');
    expect(wrapper.emitted('click')).toBeTruthy();
  });

  it.each`
    size    | expectedClass
    ${'sm'} | ${SIZE_CLASS.sm}
    ${'lg'} | ${SIZE_CLASS.lg}
  `(
    'when size prop is $size, button should have $expectedClass class',
    ({ size, expectedClass }) => {
      wrapper = createComponent({ size });
      expect(wrapper.classes()).toContain(expectedClass);
    }
  );

  it.each`
    color          | expectedClass
    ${'primary'}   | ${COLOR_CLASS.primary}
    ${'secondary'} | ${COLOR_CLASS.secondary}
  `(
    'when color prop is $color, button should have $expectedClass class',
    ({ color, expectedClass }) => {
      wrapper = createComponent({ color });
      expect(wrapper.classes()).toContain(expectedClass);
    }
  );
});
