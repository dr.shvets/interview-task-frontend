export const SIZE_CLASS = Object.freeze({
  sm: 'btn-sm',
  md: '',
  lg: 'btn-lg',
});

export const COLOR_CLASS = Object.freeze({
  primary: 'btn-primary',
  secondary: 'btn-secondary',
});
