import BusStopsList from './BusStopsList.vue';
import { mount } from '@vue/test-utils';
import VSpinner from '@/ui/v-spinner/VSpinner.vue';
import VAlert from '@/ui/v-alert/VAlert.vue';
import VTable from '@/ui/v-table/VTable.vue';

const busStops = [
  'Igołomska',
  'Jeziorko',
  'Bieńczycka',
  'Głowackiego',
  'Aleja Przyjaźni',
  'Aleja Waszyngtona',
  'Habina',
];

describe('BusStopsList', () => {
  const createComponent = (props?: typeof BusStopsList.props) => {
    return mount(BusStopsList, {
      props: {
        busStops,
        loading: false,
        error: false,
        ...props,
      },
    });
  };

  it('renders spinner when loading', () => {
    wrapper = createComponent({
      loading: true,
    });

    expect(wrapper.findComponent(VSpinner).exists()).toBe(true);
    expect(wrapper.findComponent(VTable).exists()).toBe(false);
  });

  it('renders alert when error', () => {
    wrapper = createComponent({
      error: true,
    });

    expect(wrapper.findComponent(VAlert).exists()).toBe(true);
    expect(wrapper.findComponent(VTable).exists()).toBe(false);
  });

  it('searches bus stops when search value changes', async () => {
    wrapper = createComponent();

    const table = wrapper.findComponent(VTable);
    const searchInput = wrapper.find('input[aria-label="search"]');

    searchInput.setValue('ale');
    searchInput.trigger('input');

    await wrapper.vm.$nextTick();

    const rowsContent = table
      .find('tbody')
      .findAll('tr')
      .map((row) => {
        return row.text();
      });

    expect(rowsContent).toEqual(['Aleja Przyjaźni', 'Aleja Waszyngtona']);
  });

  let wrapper: ReturnType<typeof createComponent>;
});
