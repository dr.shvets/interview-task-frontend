import BusLinesStopSchedule from './BusLinesStopSchedule.vue';
import { mount } from '@vue/test-utils';
import VTable from '@/ui/v-table/VTable.vue';

describe('BusLinesStopSchedule', () => {
  const createComponent = (props?: typeof BusLinesStopSchedule.props) => {
    return mount(BusLinesStopSchedule, {
      props: {
        busStop: undefined,
        schedule: [],
        ...props,
      },
    });
  };

  let wrapper: ReturnType<typeof createComponent>;

  it('renders empty state when no bus stop is selected', () => {
    wrapper = createComponent();

    expect(wrapper.text()).toContain('Please select the bus stop first');
  });

  it('renders bus stop schedule in ascending order when bus stop is selected', () => {
    wrapper = createComponent({
      busStop: 'Biskupa Prandoty',
      schedule: ['16:40', '19:05', '16:54'],
    });

    const table = wrapper.findComponent(VTable);

    expect(table.exists()).toBe(true);

    const rowsContent = table
      .find('tbody')
      .findAll('tr')
      .map((row) => {
        return row.text();
      });

    expect(rowsContent).toEqual(['16:40', '16:54', '19:05']);
  });

  it('renders bus stop name when bus stop is selected', () => {
    wrapper = createComponent({
      busStop: 'Biskupa Prandoty',
      schedule: ['16:40', '19:05', '16:54'],
    });

    expect(wrapper.text()).toContain('Bus Stop: Biskupa Prandoty');
  });
});
