import BusLinesNav from './BusLinesNav.vue';
import { mount } from '@vue/test-utils';
import VButton from '@/ui/v-button/VButton.vue';
import VSpinner from '@/ui/v-spinner/VSpinner.vue';
import VAlert from '@/ui/v-alert/VAlert.vue';

describe('BusLinesNav', () => {
  const createComponent = (props?: typeof BusLinesNav.props) => {
    return mount(BusLinesNav, {
      props: {
        busLineList: [101, 102, 103],
        selectedBusLine: undefined,
        loading: false,
        error: false,
        ...props,
      },
    });
  };

  let wrapper: ReturnType<typeof createComponent>;

  it('renders spinner when loading', () => {
    wrapper = createComponent({
      loading: true,
    });

    expect(wrapper.findComponent(VSpinner).exists()).toBe(true);
  });

  it('renders alert when error', () => {
    wrapper = createComponent({
      error: true,
    });

    expect(wrapper.findComponent(VAlert).exists()).toBe(true);
  });

  it('emits update:selected-bus-line with bus line number when button is clicked', () => {
    wrapper = createComponent();

    const [button] = wrapper.findAllComponents(VButton).filter((button) => {
      return button.text().includes('102');
    });

    button.trigger('click');

    expect(wrapper.emitted('update:selected-bus-line')).toEqual([[102]]);
  });
});
