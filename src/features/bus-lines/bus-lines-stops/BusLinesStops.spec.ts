import { mount } from '@vue/test-utils';
import BusLinesStops from './BusLinesStops.vue';
import VTable from '@/ui/v-table/VTable.vue';

describe('BusLinesStops', () => {
  const SELECTED_BUS_LINE = 102;
  const BUS_LINE_STOPS = [
    {
      name: 'Rzepichy',
      order: 21,
      stops: [
        { line: SELECTED_BUS_LINE, stop: 'Rzepichy', order: 21, time: '8:49' },
      ],
    },
    {
      name: 'Głowackiego',
      order: 8,
      stops: [
        {
          line: SELECTED_BUS_LINE,
          stop: 'Głowackiego',
          order: 8,
          time: '16:39',
        },
        {
          line: SELECTED_BUS_LINE,
          stop: 'Głowackiego',
          order: 8,
          time: '14:48',
        },
        {
          line: SELECTED_BUS_LINE,
          stop: 'Głowackiego',
          order: 8,
          time: '14:31',
        },
      ],
    },
  ];

  const createComponent = (props?: typeof BusLinesStops.props) => {
    return mount(BusLinesStops, {
      props: {
        busLine: undefined,
        busLineStops: [],
        selectedBusStop: undefined,
        ...props,
      },
    });
  };

  let wrapper: ReturnType<typeof createComponent>;

  it('renders empty state when no bus line is selected', async () => {
    wrapper = createComponent();

    expect(wrapper.findComponent(VTable).exists()).toBe(false);
    expect(wrapper.text()).toContain('Please select the bus stop first');
  });

  it('renders bus stops in ascending order when bus line is selected', () => {
    wrapper = createComponent({
      busLine: SELECTED_BUS_LINE,
      busLineStops: BUS_LINE_STOPS,
      selectedBusStop: undefined,
    });

    const table = wrapper.findComponent(VTable);

    expect(table.exists()).toBe(true);

    const rowsContent = table
      .find('tbody')
      .findAll('tr')
      .map((row) => {
        return row.text();
      });

    expect(rowsContent).toEqual(['Głowackiego', 'Rzepichy']);
  });

  it('emits selected bus stop when bus stop is clicked', () => {
    wrapper = createComponent({
      busLine: SELECTED_BUS_LINE,
      busLineStops: BUS_LINE_STOPS,
      selectedBusStop: undefined,
    });

    wrapper.findAll('tr').at(1)?.trigger('click');

    expect(wrapper.emitted('update:selected-bus-stop')).toEqual([
      ['Głowackiego'],
    ]);
  });

  it('renders bus line when bus line is selected', () => {
    wrapper = createComponent({
      busLine: SELECTED_BUS_LINE,
      busLineStops: BUS_LINE_STOPS,
      selectedBusStop: undefined,
    });

    expect(wrapper.text()).toContain('Bus Line: 102');
  });
});
